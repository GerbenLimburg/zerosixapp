//
//  ViewController.swift
//  zerosixapp
//
//  Created by Gerben on 04/01/2017.
//  Copyright © 2017 delinea. All rights reserved.
//

import UIKit
import GoogleMaps
import Alamofire
import SwiftyJSON


class MapViewController: UIViewController {
    
    var a = String()
    var arrRes = [[String:AnyObject]]() //Array of dictionary
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GMSServices.provideAPIKey("AIzaSyBUQYKQtAGt-HlSwhT6CmJ3YKnXt10Nm70");
        // AIzaSyBUQYKQtAGt-HlSwhT6CmJ3YKnXt10Nm70
        
        // Create a GMSCameraPosition that tells the map to display the
        // coordinate -33.86,151.20 at zoom level 6.
        
        // 52.149648, 4.455799
        // 52.1451509,4.9482996
        let camera = GMSCameraPosition.camera(withLatitude: 52.1451509, longitude: 4.9482996, zoom: 7.0)
        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
        view = mapView

        //        Alamofire.request("https://www.delinea.nl/wp-content/themes/gridstack-child/gerben/tmp.json").responseData { (resData) -> Void in
//            self.a = String(data : resData.result.value!, encoding : String.Encoding.utf8)!
//            let x = 10;
//        }
        
        Alamofire.request("https://www.delinea.nl/wp-content/themes/gridstack-child/gerben/tmp.json").responseJSON { (responseData) -> Void in
            if((responseData.result.value) != nil) {
                let swiftyJsonVar = JSON(responseData.result.value!)
                
                if let resData = swiftyJsonVar["markers"].arrayObject {
                    self.arrRes = resData as! [[String:AnyObject]]
                    
                    let count = self.arrRes.count
                    for index in 0...count - 1 {
                        var dict = self.arrRes[index];
                        
                        let lat = dict["lat"] as? Double
                        let long = dict["lon"] as? Double
                        let title = dict["title"] as? String
                        let snippet = dict["snippet"] as? String
                        
                        let marker = GMSMarker()
                        marker.position = CLLocationCoordinate2D(latitude: lat!, longitude: long!)
                        marker.title = title
                        marker.snippet = snippet
                        marker.map = mapView
                    }
                }
            }
        }
        
//        // Creates a marker in the center of the map.
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: 52.149648, longitude: 4.455799)
//        marker.title = "Studying"
//        marker.snippet = "Jess & Me"
//        marker.map = mapView
//        
//        let marker2 = GMSMarker()
//        marker2.position = CLLocationCoordinate2D(latitude: 52.435617, longitude: 6.234233)
//        marker2.title = "Delinea"
//        marker2.snippet = "Arjen, Frederik & Aad"
//        marker2.map = mapView;
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

